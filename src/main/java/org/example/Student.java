package org.example;

public class Student extends Person {
	public Student(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public Student() {
		super();
	}

	@Override
	public void print() {
		System.out.printf("Student()");
	}
}
