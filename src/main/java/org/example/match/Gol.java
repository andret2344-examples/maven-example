package org.example.match;

class Gol extends Wydarzenie {
    private boolean czyAsysta;

    public Gol(int czas, boolean czyAsysta) {
        super(czas);
        this.czyAsysta = czyAsysta;
    }

    public boolean czyAsysta() {
        return czyAsysta;
    }
}
