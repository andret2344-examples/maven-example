package org.example.match;

class StalyFragmentGry extends Wydarzenie {
    private String rodzajRzutu;

    public StalyFragmentGry(int czas, String rodzajRzutu) {
        super(czas);
        this.rodzajRzutu = rodzajRzutu;
    }

    public String rodzajRzutu() {
        return rodzajRzutu;
    }

}
