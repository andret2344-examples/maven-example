package org.example.match;

import java.util.ArrayList;
import java.util.List;

class Druzyna {
    private String nazwa;
    private String trener;
    private ArrayList<Zawodnik> zawodnicy = new ArrayList<>();

    public Druzyna(String nazwa, String trener) {
        this.nazwa = nazwa;
        this.trener = trener;
    }

    public String pobierzNazweDruzyny() {
        return nazwa;
    }

    public String pobierzTrenera() {
        return trener;
    }

    public List<Zawodnik> pobierzZawodnikow() {
        return zawodnicy;
    }

    public void dodajZawodnika(Zawodnik zawodnik) {
        zawodnicy.add(zawodnik);
    }

    public void usunZawodnika(Zawodnik zawodnik) {
        zawodnicy.remove(zawodnik);
    }
}
