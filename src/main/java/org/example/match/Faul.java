package org.example.match;

import java.util.List;

class Faul extends Wydarzenie {
    private List<Zawodnik> sfaulowany;

    public Faul(int czas, List<Zawodnik> sfaulowany) {
        super(czas);
        this.sfaulowany = sfaulowany;
    }

    public List<Zawodnik> getSfaulowany() {
        return sfaulowany;
    }
}
