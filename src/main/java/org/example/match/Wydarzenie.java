package org.example.match;

import java.util.ArrayList;
import java.util.List;

abstract class Wydarzenie {
    protected int czas;
    protected List<Zawodnik> zawodnicy;

    protected Wydarzenie(int czas) {
        this.czas = czas;
        this.zawodnicy = new ArrayList<>();
    }

    public int pobierzCzas() {
        return czas;
    }

    public List<Zawodnik> pobierzZawodnikow() {
        return zawodnicy;
    }

    public void dodajZawodnika(Zawodnik zawodnik) {
        zawodnicy.add(zawodnik);
    }

    public void usunZawodnika(Zawodnik zawodnik) {
        zawodnicy.remove(zawodnik);
    }

    @Override
    public String toString() {
        return " [wydarzenie=" + getClass().getSimpleName() + ", czas=" + czas + ", zawodnicy=" + zawodnicy + "]";
    }

}
