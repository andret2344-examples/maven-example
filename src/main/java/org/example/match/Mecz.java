package org.example.match;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class Mecz {
    private Druzyna druzynaGospodarzy;
    private Druzyna druzynaGoscia;
    private String wynik = "";
    private List<Wydarzenie> wydarzenia = new ArrayList<>();
    private String rodzaj = "";
    private Date data;
    private String sedzia = "";

    public Mecz(Druzyna druzynaGospodarzy, Druzyna druzynaGoscia) {
        this(druzynaGospodarzy, druzynaGoscia, new Date());
    }

    public Mecz(Druzyna druzynaGospodarzy, Druzyna druzynaGoscia, Date data) {
        this.druzynaGospodarzy = druzynaGospodarzy;
        this.druzynaGoscia = druzynaGoscia;
        this.data = data;
    }

    public Druzyna pobierzDruzyneGospodarzy() {
        return druzynaGospodarzy;
    }

    public Druzyna pobierzDruzyneGoscia() {
        return druzynaGoscia;
    }

    public String pobierzWynik() {
        return wynik;
    }

    public void ustawWynik(String wynik) {
        this.wynik = wynik;
    }

    public Date pobierzDate() {
        return data;
    }

    public void ustawDate(Date data) {
        this.data = data;
    }

    public List<Wydarzenie> pobierzWydarzenia() {
        return wydarzenia;
    }

    public void dodajWydarzenie(Wydarzenie wydarzenie) {
        wydarzenia.add(wydarzenie);
    }

    public void usunWydarzenie(Wydarzenie wydarzenie) {
        wydarzenia.remove(wydarzenie);
    }

    public void dodajRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public void dodajSedzie(String sedzia) {
        this.sedzia = sedzia;
    }

    public String pobierzSedzie() {
        return sedzia;
    }
}
