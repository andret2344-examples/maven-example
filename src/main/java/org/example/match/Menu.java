package org.example.match;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
	private static final Scanner scanner = new Scanner(System.in);
	private static Mecz aktualnyMecz;

	public static void main(String[] args) {
		boolean zakonczono = false;

		while (!zakonczono) {
			wyswietlOpcje();
			int wybor = scanner.nextInt();
			scanner.nextLine(); // Pobierz nową linię po wczytaniu liczby

			switch (wybor) {
				case 1:
					rozpocznijNowyMecz();
					dodajZawodnikow();
					break;
				case 2:
					dodajWydarzenie();
					break;
				case 3:
					wyswietlPodsumowanie();
					break;
				case 4:
					zakonczMecz();
					break;
				case 5:
					dodajZawodnikow();
					break;
				case 6:
					zakonczono = true;
					break;
				case 7:
					usunZawodnika();
					break;
				default:
					System.out.println("Nieprawidłowy wybór. Spróbuj ponownie.");
					break;
			}
		}

		System.out.println("Program zakończony.");
	}

	private static void wyswietlOpcje() {
		System.out.println("----- MENU -----");
		System.out.println("1. Rozpocznij nowy mecz");
		System.out.println("2. Dodaj wydarzenie");
		System.out.println("3. Wyświetl podsumowanie meczu");
		System.out.println("4. Zakończ mecz");
		System.out.println("5. Dodaj zawodnika");
		System.out.println("6. Zakończ program");
		System.out.println("7. Usun zawodnika");
		System.out.println("Wybierz opcję: ");
	}

	private static void rozpocznijNowyMecz() {
		System.out.println("Podaj nazwę drużyny gospodarzy: ");
		String nazwaGospodarzy = scanner.nextLine();
		System.out.println("Podaj trenera drużyny gospodarzy: ");
		String trenerGospodarzy = scanner.nextLine();

		System.out.println("Podaj nazwę drużyny gości: ");
		String nazwaGosci = scanner.nextLine();
		System.out.println("Podaj trenera drużyny gosci: ");
		String trenerGosci = scanner.nextLine();

		Druzyna druzynaGospodarzy = new Druzyna(nazwaGospodarzy, trenerGospodarzy);
		Druzyna druzynaGosci = new Druzyna(nazwaGosci, trenerGosci);

		aktualnyMecz = new Mecz(druzynaGospodarzy, druzynaGosci);

		System.out.println("Podaj sedziego: ");
		String sedzia = scanner.nextLine();
		aktualnyMecz.dodajSedzie(sedzia);

		System.out.println("Rozpoczęto nowy mecz: " + nazwaGospodarzy + " vs " + nazwaGosci);
		System.out.println("Sedzia: " + sedzia);
	}

	public static void dodajZawodnikow() {
		System.out.print("Podaj liczbę dodawanych zawodników: ");
		int liczbaZawodnikow = scanner.nextInt();

		System.out.print("Do jakiej drużyny dodajesz zawodników? (1 - Drużyna gospodarzy, 2 - Drużyna gości): ");
		int wybor1 = scanner.nextInt();

		switch (wybor1) {
			case 1:
				for (int i = 0; i < liczbaZawodnikow; i++) {
					System.out.print("Podaj imię zawodnika: ");
					String imie = scanner.next();
					System.out.print("Podaj nazwisko zawodnika: ");
					String nazwisko = scanner.next();
					System.out.print("Podaj pozycję zawodnika: ");
					String pozycja = scanner.next();

					Zawodnik zawodnik = new Zawodnik(imie, nazwisko, pozycja);
					aktualnyMecz.pobierzDruzyneGospodarzy().dodajZawodnika(zawodnik);
				}

				System.out.println("Dodano zawodników do drużyny gospodarzy.");
				for (Zawodnik zawodnik : aktualnyMecz.pobierzDruzyneGospodarzy().pobierzZawodnikow()) {
					System.out.println(zawodnik.toString());
				}
				break;

			case 2:
				for (int i = 0; i < liczbaZawodnikow; i++) {
					System.out.print("Podaj imię zawodnika: ");
					String imie = scanner.next();
					System.out.print("Podaj nazwisko zawodnika: ");
					String nazwisko = scanner.next();
					System.out.print("Podaj pozycję zawodnika: ");
					String pozycja = scanner.next();

					Zawodnik zawodnik = new Zawodnik(imie, nazwisko, pozycja);
					aktualnyMecz.pobierzDruzyneGoscia().dodajZawodnika(zawodnik);
				}

				System.out.println("Dodano zawodników do drużyny gości.");
				for (Zawodnik zawodnik : aktualnyMecz.pobierzDruzyneGoscia().pobierzZawodnikow()) {
					System.out.println(zawodnik.toString());
				}
				break;

			default:
				System.out.println("Nieprawidłowy wybór. Zawodnicy nie zostali dodani do żadnej drużyny.");
				break;
		}
	}

	private static void dodajWydarzenie() {
		if (aktualnyMecz == null) {
			System.out.println("Najpierw rozpocznij nowy mecz.");
			return;
		}

		System.out.println("Wybierz rodzaj wydarzenia:");
		System.out.println("1. Spalony");
		System.out.println("2. Stały fragment gry");
		System.out.println("3. Gol");
		System.out.println("4. Kartka");
		System.out.println("5. Faul");
		System.out.println("Wybierz opcję: ");
		int wybor = scanner.nextInt();
		scanner.nextLine(); // Pobierz nową linię po wczytaniu liczby

		System.out.println("Podaj czas wydarzenia: ");
		int czas = scanner.nextInt();
		scanner.nextLine(); // Pobierz nową linię po wczytaniu liczby

        List<Zawodnik> zawodnicy = znajdzZawodnikow();

        Wydarzenie wydarzenie;
        switch (wybor) {
			case 1:
                wydarzenie = new Spalony(czas);
				for (Zawodnik zawodnik : zawodnicy) {
					wydarzenie.dodajZawodnika(zawodnik);
				}
                aktualnyMecz.dodajWydarzenie(wydarzenie);
				break;
			case 2:
				System.out.println("Podaj rodzaj rzutu: ");
				String rodzajRzutu = scanner.next();
                wydarzenie = new StalyFragmentGry(czas, rodzajRzutu);
				for (Zawodnik zawodnik : zawodnicy) {
					wydarzenie.dodajZawodnika(zawodnik);
				}
                aktualnyMecz.dodajWydarzenie(wydarzenie);
				break;
			case 3:
				System.out.println("Czy była asysta? (T/N)");
				String czyAsystaStr = scanner.nextLine();
				boolean czyAsysta = czyAsystaStr.equalsIgnoreCase("T");
                wydarzenie = new Gol(czas, czyAsysta);
				for (Zawodnik zawodnik : zawodnicy) {
					wydarzenie.dodajZawodnika(zawodnik);
				}
                aktualnyMecz.dodajWydarzenie(wydarzenie);
				break;
			case 4:
				System.out.println("Podaj rodzaj przewinienia: ");
				String rodzajPrzewinienia = scanner.nextLine();
                wydarzenie = new Kartka(czas, rodzajPrzewinienia);
				for (Zawodnik zawodnik : zawodnicy) {
					wydarzenie.dodajZawodnika(zawodnik);
				}
                aktualnyMecz.dodajWydarzenie(wydarzenie);
				break;
			case 5:
				System.out.println("Podaj liczbę sfaulowanych zawodników: ");
				int liczbaSfaulowanych = scanner.nextInt();
				scanner.nextLine(); // Pobierz nową linię po wczytaniu liczby

				List<Zawodnik> sfaulowani = new ArrayList<>();
				for (int i = 0; i < liczbaSfaulowanych; i++) {
					System.out.println("Podaj nazwę sfaulowanego zawodnika: ");
					String imie = scanner.nextLine();
					String nazwisko = scanner.nextLine();
					String pozycja = scanner.nextLine();
					Zawodnik zawodnik = new Zawodnik(imie, nazwisko, pozycja);
					sfaulowani.add(zawodnik);
				}

                wydarzenie = new Faul(czas, sfaulowani);
				for (Zawodnik zawodnik : zawodnicy) {
					wydarzenie.dodajZawodnika(zawodnik);
				}
                aktualnyMecz.dodajWydarzenie(wydarzenie);
				break;
			default:
				System.out.println("Nieprawidłowy wybór. Wydarzenie nie zostało dodane.");
				break;
		}

		System.out.println("Wydarzenie dodane.");
	}

	private static List<Zawodnik> znajdzZawodnikow() {
		List<Zawodnik> zawodnicy = new ArrayList<>();
		System.out.println("Ilosc zawodnikow: ");
		int ilosc = scanner.nextInt();
		for (int i = 0; i < ilosc;) {
			Zawodnik zawodnik = znajdzZawodnika();
			if (zawodnik != null) {
				zawodnicy.add(zawodnik);
				i++;
			} else {
				System.out.println("Niepoprawny zawodnik!");
			}
		}
        return zawodnicy;
	}

	private static Zawodnik znajdzZawodnika() {
		System.out.println("Podaj imie zawodnika: ");
		String imie = scanner.next();
		System.out.println("Podaj nazwisko zawodnika: ");
		String nazwisko = scanner.next();
		scanner.nextLine();
		for (Zawodnik zawodnik : aktualnyMecz.pobierzDruzyneGospodarzy().pobierzZawodnikow()) {
			if (zawodnik.pobierzImie().equals(imie) && zawodnik.pobierzNazwisko().equals(nazwisko)) {
				return zawodnik;
			}
		}
		for (Zawodnik zawodnik : aktualnyMecz.pobierzDruzyneGoscia().pobierzZawodnikow()) {
			if (zawodnik.pobierzImie().equals(imie) && zawodnik.pobierzNazwisko().equals(nazwisko)) {
				return zawodnik;
			}
		}
		return null;
	}

	private static void wyswietlPodsumowanie() {
		if (aktualnyMecz == null) {
			System.out.println("Najpierw rozpocznij nowy mecz.");
			return;
		}
		System.out.println("Czego podsumowanie wyświetlić? 1 - Mecz, 2 - Druzyna, 3 - Zawodnik");
		int wybor = scanner.nextInt();
		switch (wybor) {
			case 1:
				System.out.println(Statystyki.generujPodsumowanieMeczu(aktualnyMecz));
				break;
			case 2:
				System.out.println("Ktora druzyna? 1 - Gospodarze, 2 - Goscie");
				int druzyna = scanner.nextInt();
				switch (druzyna) {
					case 1:
						System.out.println(Statystyki.generujStatystykiDruzyny(aktualnyMecz, aktualnyMecz.pobierzDruzyneGospodarzy()));
						break;
					case 2:
						System.out.println(Statystyki.generujStatystykiDruzyny(aktualnyMecz, aktualnyMecz.pobierzDruzyneGoscia()));
						break;
					default:
						System.out.println("Bledny wybor");
						return;
				}
				break;
			case 3:
				Zawodnik zawodnik = znajdzZawodnika();
				if (zawodnik != null) {
					System.out.println(Statystyki.generujStatystykiZawodnika(aktualnyMecz, zawodnik));
				} else {
					System.out.println("Niepoprawny zawodnik");
				}
				break;
			default:
				System.out.println("Niepoprawny wybor.");
		}
	}

	private static void zakonczMecz() {
		if (aktualnyMecz == null) {
			System.out.println("Najpierw rozpocznij nowy mecz.");
			return;
		}

		System.out.println("Podaj wynik meczu: ");
		String wynik = scanner.nextLine();
		aktualnyMecz.ustawWynik(wynik);

		System.out.println("Mecz zakończony. Wynik: " + wynik);

		aktualnyMecz = null;
	}

	private static void usunZawodnika() {
		Zawodnik zawodnik = znajdzZawodnika();
		if (zawodnik == null) {
			System.out.println("Niepoprawny zawodnik");
		}
		if (aktualnyMecz.pobierzDruzyneGospodarzy().pobierzZawodnikow().contains(zawodnik)) {
			aktualnyMecz.pobierzDruzyneGospodarzy().usunZawodnika(zawodnik);
			System.out.println("Usunieto zawodnika z druzyny gospodarzy");
			return;
		}
		if (aktualnyMecz.pobierzDruzyneGoscia().pobierzZawodnikow().contains(zawodnik)) {
			aktualnyMecz.pobierzDruzyneGoscia().usunZawodnika(zawodnik);
			System.out.println("Usunieto zawodnika z druzyny gosci");
			return;
		}
		System.out.println("Zawodnik nie w druzynie!");
	}
}
