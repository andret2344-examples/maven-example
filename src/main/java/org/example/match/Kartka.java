package org.example.match;

class Kartka extends Wydarzenie {
    private String rodzajPrzewinienia;

    public Kartka(int czas, String rodzajPrzewinienia) {
        super(czas);
        this.rodzajPrzewinienia = rodzajPrzewinienia;
    }

    public String rodzajPrzewinienia() {
        return rodzajPrzewinienia;
    }
}
