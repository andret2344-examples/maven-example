package org.example.match;

public class Statystyki {
    public static String generujPodsumowanieMeczu(Mecz mecz) {
        StringBuilder podsumowanie = new StringBuilder();
        podsumowanie.append("Podsumowanie meczu:\n");
        podsumowanie.append("Sedzia: ").append(mecz.pobierzSedzie()).append("\n");
        podsumowanie.append("Drużyna gospodarzy: Trener: ")
                .append(mecz.pobierzDruzyneGospodarzy().pobierzTrenera())
                .append("\tNazwa: ")
                .append(mecz.pobierzDruzyneGospodarzy().pobierzNazweDruzyny())
                .append("\n");
        podsumowanie.append("Drużyna gości: Trener: ")
                .append(mecz.pobierzDruzyneGoscia().pobierzTrenera())
                .append("\tNazwa: ")
                .append(mecz.pobierzDruzyneGoscia().pobierzNazweDruzyny())
                .append("\n");
        podsumowanie.append("Wynik: ").append(mecz.pobierzWynik()).append("\n");
        podsumowanie.append("Data: ").append(mecz.pobierzDate()).append("\n");
        podsumowanie.append("Wydarzenia: ").append("\n");
        for (Wydarzenie wydarzenie : mecz.pobierzWydarzenia()) {
            podsumowanie.append(wydarzenie).append("\n");
        }
        return podsumowanie.toString();
    }

    public static String generujStatystykiDruzyny(Mecz mecz, Druzyna druzyna) {
        StringBuilder statystyki = new StringBuilder();
        statystyki.append("Statystyki drużyny: ").append(druzyna.pobierzNazweDruzyny()).append("\n");
        statystyki.append("Trener: ").append(druzyna.pobierzTrenera()).append("\n");
        statystyki.append("Zawodnicy: ").append("\n");
        for (Zawodnik zawodnik : druzyna.pobierzZawodnikow()) {
            statystyki.append(zawodnik).append("\n");
            for (Wydarzenie wydarzenie : mecz.pobierzWydarzenia()) {
                if (wydarzenie.pobierzZawodnikow().contains(zawodnik)) {
                    statystyki.append(wydarzenie).append("\n");
                }
            }
        }
        return statystyki.toString();
    }

    public static String generujStatystykiZawodnika(Mecz mecz, Zawodnik zawodnik) {
        StringBuilder statystyki = new StringBuilder();
        statystyki.append("Statystyki zawodnika: ").append(zawodnik.pobierzImie()).append(" ").append(zawodnik.pobierzNazwisko()).append("\n");
        statystyki.append("Pozycja: ").append(zawodnik.pobierzPozycje()).append("\n");
        statystyki.append("Kraj pochodzenia: ").append(zawodnik.pobierzKrajPochodzenia()).append("\n");
        statystyki.append("Wiek: ").append(zawodnik.pobierzWiek()).append("\n");
        for (Wydarzenie wydarzenie : mecz.pobierzWydarzenia()) {
            if (wydarzenie.pobierzZawodnikow().contains(zawodnik)) {
                statystyki.append(wydarzenie).append("\n");
            }
        }
        return statystyki.toString();
    }

    @Override
    public String toString() {
        return "Statystyki";
    }
}
