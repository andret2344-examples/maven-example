package org.example.match;

class Zawodnik {
    private String imie;
    private String nazwisko;
    private int wiek;
    private String krajPochodzenia;
    private String pozycja;

    public Zawodnik(String imie, String nazwisko, String pozycja) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pozycja = pozycja;
    }

    public String pobierzImie() {
        return imie;
    }

    public String pobierzNazwisko() {
        return nazwisko;
    }

    public int pobierzWiek() {
        return wiek;
    }

    public String pobierzKrajPochodzenia() {
        return krajPochodzenia;
    }

    public String pobierzPozycje() {
        return pozycja;
    }

    public void ustawPozycje(String pozycja) {
        this.pozycja = pozycja;
    }

    public String toString() {
        return "Imię: " + imie + ", Nazwisko: " + nazwisko + ", Pozycja: " + pozycja;
    }
}
