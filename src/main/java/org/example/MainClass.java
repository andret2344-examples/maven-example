package org.example;

public class MainClass {
    public static void main(String[] args) {
        String message = getMessage(args);
        System.out.println(message);
    }

    private static String getMessage(String[] args) {
        if (args.length == 0) {
            return "Welcome!";
        }
        if (args.length != 1) {
            return "invalid command";
        }
        if (args[0].equalsIgnoreCase("run")) {
            return "running";
        }
        if (args[0].equalsIgnoreCase("stop")) {
            return "stopping";
        }
        return "invalid command";
    }

    public static String validateName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("The name is null");
        }
        if (name.isEmpty()) {
            throw new IllegalArgumentException("The name is empty");
        }
        return name;
    }
}
