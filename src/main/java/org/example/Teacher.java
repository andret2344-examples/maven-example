package org.example;

import java.util.Objects;

public class Teacher extends Person {
	private String subject;

	public Teacher(String firstName, String lastName, String subject) {
		super(firstName, lastName);
		this.subject = subject;
	}

	public Teacher(String subject) {
		super();
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public void print() {
		System.out.printf("Teacher(%s %s, %s)%n", getFirstName(), getLastName(), subject);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Teacher teacher = (Teacher) o;
		return Objects.equals(subject, teacher.subject);
	}

	@Override
	public int hashCode() {
		return Objects.hash(subject);
	}

	@Override
	public String toString() {
		return "Teacher{" +
				"subject='" + subject + '\'' +
				"} " + super.toString();
	}
}
